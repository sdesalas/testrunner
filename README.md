TESTRUNNER
======

An Implementation of the TestRunner Testing Language for websites and web applications.

    ' file: /tests/1.Login.test

    TEST SUITE "Bobs Hardware Administrator Login"
    
    SET SCREENSIZE TO 800 BY 600

    TEST "Log into Bobs hardware"
	
	SET @url = "http://local.bobs-hardware.com/admin"
    
    GO TO @url
    CLICK "input[name=user]" AND WRITE "bobtest"
	CLICK "input[name=passw]" AND WRITE "secret"
	CLICK "button:contains('Login')"
	
    CHECK "div#wrapper H3.welcome" TEXT CONTAINS "Welcome Bob"
	
    TAKE SCREENSHOT
    
    GO

To execute:

    $ run.bat tests/1.Login.test @url=http://dev.bobs-hardware.com/admin